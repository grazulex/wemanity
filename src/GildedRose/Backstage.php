<?php

namespace GildedRose;

class Backstage extends Item
{
    public function updateQuality()
    {
        $this->quality += 1;
        //plus de 10 jours de vente, on ajoute un quality (total = 2)
        if ($this->sell_In <= 10) {
            $this->quality += 1;
        }

	//plus de 5 jours de vente, on ajoute un quality (total = 3)
        if ($this->sell_In <= 5) {
            $this->quality += 1;
        }

	//max quality=50
        if ($this->quality > 50) {
            $this->quality = 50;
        }

	//si jour de vente = 0 alors quality = 0
        if ($this->sell_In <= 0) {
            $this->quality = 0;
        }

        $this->sell_In -= 1;
    }
}
