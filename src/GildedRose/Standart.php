<?php

namespace GildedRose;

class Standart extends Item
{
    public function updateQuality()
    {
        $this->quality -= 1;
        $this->sell_In -= 1;

        if ($this->sell_In <= 0) {
            $this->quality -= 1;
        }
    }
}