<?php

namespace GildedRose;

class Item
{
    public $name;
    public $sell_In;
    public $quality;

    public function __construct($name, $sell_In, $quality)
    {
        $this->name = $name;
        $this->sell_In = $sell_In;
        $this->quality = $quality;
    }

    public function __toString() {
        return "{$this->name}, {$this->sell_In}, {$this->quality}";
    }
}
