<?php

namespace GildedRose;

class GildedRose
{
    public static $lookup = [
        'Aged Brie' => AgedBrie::class,
        'Backstage passes to a TAFKAL80ETC concert' => Backstage::class,
        'Sulfuras, Hand of Ragnaros' => Sulfuras::class,
        'Conjured Mana Cake' => Conjured::class,
    ];
    
    public static function type($name, $quality, $sell_In)
    {
        if (array_key_exists($name, self::$lookup)) {
            return new self::$lookup[$name]($name, $sell_In, $quality);
        }
        return new Standart($name, $sell_In, $quality);
    }
}
